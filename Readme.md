# Chauffeur
Chauffeur is a tool to help manage drivers for automating browsers.

It has two primary behaviors: installing drivers and adding them to your path.

_!!Note: Microsoft WebDriver will only work for version 14393 and greater. Before that it used an installer._

## Usage
This is very important! Chauffeur must be run from the working directory of the project or it will not function.

Add it to Gemfile or use `gem install chauffeur`

Initialize drivers in your project with
```
cmd > chauffeur init
```
This will create a directory in your project with a file config.yml and the folders to hold drivers.

####driver_versions:
takes no arguments and prints the installed versions for each browser.
```
cmd > chauffeur driver_versions
    {"chromedriver"=>
      {"linux32"=>"nil", "linux64"=>"nil", "mac32"=>"nil", "win32"=>"nil"},
     "geckodriver"=>
      {"linux32"=>"nil",
       "linux64"=>"nil",
       "macos"=>"nil",
       "win32"=>"nil",
       "win64"=>"nil"},
     "iedriver"=>{"Win32"=>"nil", "x64"=>"nil"},
     "microsoft_webdriver"=>{}}
```
###upgrade_driver:
takes the browser name and system platform as arguments and installs the driver.
        browser: chrome, firefox, ie, edge
        platform:   chrome: [linux32, linux64, mac32, win32]
                    firefox: [linux32, linux64, macos, win32, win64]
                    ie: [Win32, x64]
                    edge: build version number
```
cmd > chauffeur upgrade_driver ie Win32
     installing 'http://selenium-release.storage.googleapis.com/3.8/IEDriverServer_Win32_3.8.0.zip' into 'C:/path/to/project/drivers/iedriver/Win32/'
```
###upgrade_driver_all_platforms:
takes the browser name and installs most current driver for each platform
        browser: chrome, firefox, ie, edge

```
cmd > chauffeur upgrade_driver_all_platforms chrome
    installing 'http://chromedriver.storage.googleapis.com/2.33/chromedriver_linux32.zip' into 'C:/path/to/project/drivers/chromedriver/linux32/'
    installing 'http://chromedriver.storage.googleapis.com/2.34/chromedriver_linux64.zip' into 'C:/path/to/project/drivers/chromedriver/linux64/'
    installing 'http://chromedriver.storage.googleapis.com/2.22/chromedriver_mac32.zip' into 'C:/path/to/project/drivers/chromedriver/mac32/'
    installing 'http://chromedriver.storage.googleapis.com/2.34/chromedriver_win32.zip' into 'C:/path/to/project/drivers/chromedriver/win32/'
```

###install_driver:
takes browser, platform, and version and installs that driver.
        browser: chrome, firefox, ie, edge
        platform:   chrome: [linux32, linux64, mac32, win32]
                    firefox: [linux32, linux64, macos, win32, win64]
                    ie: [Win32, x64]
                    edge: build version number
        version: must match exactly the download version

```
cmd > chauffeur install_driver firefox linux64 0.18.0
      installing 'https://github.com/mozilla/geckodriver/releases//download/v0.18.0/geckodriver-v0.18.0-linux64.tar.gz' into 'C:/path/to/project/drivers/geckodriver/linux64/'
```

###install_driver_all_platforms:
takes browser name and version number and installs that version for all platforms.
        browser: chrome, firefox, ie, edge
        version: must match exactly the download version

```
cmd > chauffeur install_driver_all_platforms chrome 2.22
    installing 'http://chromedriver.storage.googleapis.com/2.22/chromedriver_linux32.zip' into 'C:/path/to/project/drivers/chromedriver/linux32/'
    installing 'http://chromedriver.storage.googleapis.com/2.22/chromedriver_linux64.zip' into 'C:/path/to/project/drivers/chromedriver/linux64/'
    installing 'http://chromedriver.storage.googleapis.com/2.22/chromedriver_mac32.zip' into 'C:/path/to/project/drivers/chromedriver/mac32/'
    installing 'http://chromedriver.storage.googleapis.com/2.22/chromedriver_win32.zip' into 'C:/path/to/project/drivers/chromedriver/win32/'
```


### Add drivers to path:

In your project code use

```
require 'chauffeur'
Chauffeur.add_drivers_to_path
```


