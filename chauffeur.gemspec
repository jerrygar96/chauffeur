# Gemspec file for Chauffeur Gem
require 'English'

Gem::Specification.new do |s|
  s.name = 'chauffeur'
  s.version = '0.0.7'
  s.date = '2018-10-11'
  s.summary = 'summary'
  s.description = 'Chauffeur is a tool to help manage drivers for automating browsers.'
  s.authors = ['Jeremy Gardner']
  s.email = 'jerrygar96@gmail.com'
  s.files = `git ls-files`.split($ORS)
  s.homepage = 'https://gitlab.com/jerrygar96/chauffeur'
  s.license = 'MIT'
  s.executables = %w[chauffeur]

  s.add_development_dependency('pry', ['~> 0.11'])
  s.add_development_dependency('rspec', ['~> 3.7'])
  s.add_development_dependency('simplecov', ['~> 0.15'])

  s.add_runtime_dependency('archive-zip', ['~> 0.10'])
  s.add_runtime_dependency('httparty', ['~> 0.15'])
  s.add_runtime_dependency('nokogiri', '~> 1.8')
end
