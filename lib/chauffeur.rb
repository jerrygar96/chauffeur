# Chauffeur adds WebDriver executables to the path
#   Drivers must be in path '{working_directory}/features/support/drivers/{os}/'
#   otherwise, the path must be passed in.
class Chauffeur
  require_relative './chauffeur/requires'

  class << self
    # Initialize method must be run before anything else will work.
    def initialize_chauffeur
      Setup.create_folders_and_config
    end

    # Upgrades path to include the folder containing web drivers.
    def add_drivers_to_path
      path = ENV['PATH']
      new_paths = PathExpander.paths_to_add
      new_paths.reject! { |p| path.include?(p) }
      ENV['PATH'] = "#{new_paths.join(Gem.path_separator)}#{Gem.path_separator}#{path}"
    end

    #  Returns a hash of the currently installed drivers.
    def driver_versions
      config_file_path = "#{Dir.pwd}/drivers/config.yml"
      File.open(config_file_path) { |f| YAML.safe_load(f) }['installed_versions']
    end

    # Installs the latest version of chromedriver for the specified platform.
    # browser  - string, chrome, edge, firefox, ie
    # platform - string, must match for browser
    #   chrome: linux32, linux64, mac32, win32
    #   edge: win
    #   firefox: linux32, linux64, macos, win32, win64
    #   ie: Win32, x64
    def upgrade_driver(browser, platform, verbose = true)
      raise "Unknown Browser '#{browser}'." unless valid_browser?(browser)
      downloader = downloader_for(browser, verbose)
      downloader.upgrade_driver(platform)
    end

    # Installs the latest version of specified driver for all platforms.
    # browser  - string, chrome, edge, firefox, ie
    def upgrade_driver_all_platforms(browser, verbose = true)
      raise "Unknown Browser '#{browser}'." unless valid_browser?(browser)
      downloader = downloader_for(browser, verbose)
      downloader.upgrade_driver_all_platforms
    end

    # Installs the specified version of specified driver for the specified platform.
    # browser  - string, chrome, edge, firefox, ie
    # platform - string, linux32, linux 64, mac32, win32
    # version: string - must match exactly the version in the download URL
    def install_driver(browser, platform, version, verbose = true)
      raise "Unknown Browser '#{browser}'." unless valid_browser?(browser)
      downloader = downloader_for(browser, verbose)
      downloader.install_driver(version, platform)
    end

    # Installs the specified version of specified driver for all platforms.
    # browser  - string, chrome, edge, firefox, ie
    # platforms - linux32, linux 64, mac32, win32
    def install_driver_all_platforms(browser, version, verbose = true)
      raise "Unknown Browser '#{browser}'." unless valid_browser?(browser)
      downloader = downloader_for(browser, verbose)
      downloader.install_driver_all_platforms(version)
    end

    # Returns a new downloader for the specified browser.
    # browser: string - chrome, edge, firefox, ie
    def downloader_for(browser, verbose = true)
      Object.const_get("#{browser.capitalize}driverDownloader").new(verbose)
    end

    private

    def valid_browsers
      %w[chrome firefox edge ie]
    end

    def valid_browser?(browser)
      valid_browsers.include?(browser)
    end
  end
end
