# ChromeDriver specific functions for driver downloading.
class ChromedriverDownloader < DriverDownloader
  CHROMEDRIVER_URL = 'http://chromedriver.storage.googleapis.com/'.freeze

  def initialize(verbose = true)
    @all_driver_versions = all_driver_versions
    super(verbose)
  end

  def browser_name
    'chromedriver'
  end

  def driver_url
    CHROMEDRIVER_URL
  end

  def all_platforms
    %w[linux32 linux64 mac32 win32]
  end

  # Returns the most recent version of chromedriver for the
  # desired platform.
  # platform must be one of: linux32, linux64, mac32, win32
  def latest_driver_version(platform)
    raise unknown_platform_error(platform) unless valid_platform?(platform)
    platform_drivers = @all_driver_versions.select { |v| v.include?(platform) }
    platform_drivers.map { |v| version_of(v.split('/')[0]) }.max
  end

  # Returns all available versions of Chromedriver
  def all_driver_versions
    resp = HTTParty.get(CHROMEDRIVER_URL).parsed_response
    resp['ListBucketResult']['Contents'].map { |c| c['Key'] }
  end

  # Returns the url for the desired version of chromedriver
  # version: string - must match exactly the version in the download URL
  # platform: string - must be one of: linux32, linux64, mac32, win32
  def driver_download_url(version, platform)
    raise unknown_platform_error(platform) unless valid_platform?(platform)
    rel_path = @all_driver_versions.find do |v|
      v.eql?("#{version}/chromedriver_#{platform}.zip")
    end
    raise unknown_version_error(version) unless rel_path
    "#{CHROMEDRIVER_URL}#{rel_path}"
  end
end
