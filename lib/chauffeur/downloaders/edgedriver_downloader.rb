# FireFoxDriver specific functions for driver downloading.
class EdgedriverDownloader < DriverDownloader
  EDGEDRIVER_URL = 'https://developer.microsoft.com/en-us/microsoft-edge/tools/webdriver/'.freeze

  def initialize(verbose = true)
    @all_driver_versions = all_driver_versions
    super(verbose)
  end

  def browser_name
    'microsoft_webdriver'
  end

  def driver_url
    EDGEDRIVER_URL
  end

  def all_platforms
    %w[win]
  end

  # Returns the most recent version of edgedriver for the
  # desired platform.
  # platform must be one of: win
  def latest_driver_version(platform)
    raise unknown_platform_error(platform) unless valid_platform?(platform)
    @all_driver_versions.keys.sort.reverse.find { |k| k =~ /^\d+$/ }
  end

  # Returns all available versions of microsoft webdriver
  def all_driver_versions
    resp = HTTParty.get(driver_url, verify: false).parsed_response
    doc = Nokogiri::XML.parse(resp)
    output = {}
    doc.css('li.driver-download a.subtitle').each do |a|
      output[a.text.gsub('Release ', '')] = a[:href]
    end
    output
  end

  # Returns the url for the desired version of microsoft webdriver
  # version: string - must match exactly the version in the download URL
  # platform: string - must be win
  def driver_download_url(version, platform)
    raise unknown_platform_error(platform) unless valid_platform?(platform)
    @all_driver_versions[version] || raise(unknown_version_error(version))
  end
end
