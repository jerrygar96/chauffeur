# FireFoxDriver specific functions for driver downloading.
class FirefoxdriverDownloader < DriverDownloader
  GECKODRIVER_URL = 'https://github.com/mozilla/geckodriver/releases/'.freeze

  def initialize(verbose = true)
    @all_driver_versions = all_driver_versions
    super(verbose)
  end

  def browser_name
    'geckodriver'
  end

  def driver_url
    GECKODRIVER_URL
  end

  def all_platforms
    %w[linux32 linux64 macos win32 win64]
  end

  # Returns the most recent version of geckodriver for the
  # desired platform.
  # platform must be one of: linux32, linux64, mac32, win32
  def latest_driver_version(platform)
    raise unknown_platform_error(platform) unless valid_platform?(platform)
    @all_driver_versions.map { |v| Gem::Version.new(v.delete('v')) }.max
  end

  # Returns all available versions of geckodriver
  def all_driver_versions
    resp = HTTParty.get(GECKODRIVER_URL, verify: false).parsed_response
    doc = Nokogiri::XML.parse(resp)
    ver_array = doc.css('div.release div.release-header div a').map(&:text)
    raise 'No versions found' if ver_array.empty?
    ver_array - ['Latest release']
  end

  # Returns the url for the desired version of geckodriver
  # version: string - must match exactly the version in the download URL
  # platform: string - must be one of: linux32, linux64, mac32, win32
  def driver_download_url(version, platform)
    raise unknown_platform_error(platform) unless valid_platform?(platform)
    raise unknown_version_error(version) unless valid_version?(version)
    extension = platform.start_with?('win') ? 'zip' : 'tar.gz'
    rel_path = "/download/v#{version}/geckodriver-v#{version}-#{platform}.#{extension}"
    "#{GECKODRIVER_URL}#{rel_path}"
  end

  def valid_version?(version)
    valid_versions = @all_driver_versions
    valid_versions.include?("v#{version}")
  end
end
