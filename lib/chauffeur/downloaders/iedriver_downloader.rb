# IEDriver specific functions for driver downloading.
class IedriverDownloader < DriverDownloader
  IEDRIVER_URL = 'http://selenium-release.storage.googleapis.com/'.freeze

  def initialize(verbose = true)
    @all_driver_versions = all_driver_versions
    super(verbose)
  end

  def browser_name
    'iedriver'
  end

  def driver_url
    IEDRIVER_URL
  end

  def all_platforms
    %w[Win32 x64]
  end

  # Returns the most recent version of iedriver for the
  # desired platform.
  # platform must be one of: linux32, linux64, mac32, win32
  def latest_driver_version(platform)
    raise unknown_platform_error(platform) unless valid_platform?(platform)
    platform_drivers = @all_driver_versions.select { |v| v.include?(platform) }
    platform_drivers.map { |v| version_of(v.split('/')[0]) }.max
  end

  # Returns all available versions of iedriver
  def all_driver_versions
    resp = HTTParty.get(IEDRIVER_URL).parsed_response
    contents = resp['ListBucketResult']['Contents']
    contents.map { |c| c['Key'] }.select { |url| url.include?('IEDriverServer') }
  end

  # Returns the url for the desired version of iedriver
  # version: string - must match exactly the version in the download URL
  # platform: string - must be one of: linux32, linux64, mac32, win32
  def driver_download_url(version, platform)
    raise unknown_platform_error(platform) unless valid_platform?(platform)
    rel_path = @all_driver_versions.find do |v|
      v =~ %r{#{version}/IEDriverServer_#{platform}_#{version}\.\d+\.zip}
    end
    raise unknown_version_error(version) unless rel_path
    "#{IEDRIVER_URL}#{rel_path}"
  end
end
