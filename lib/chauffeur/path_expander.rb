class Chauffeur
  # Adds locations of drivers for the current platform to the beginning of the path
  module PathExpander
    def self.paths_to_add
      all_paths = [path_to_chromedriver,
                   path_to_geckodriver,
                   path_to_iedriver,
                   path_to_edgedriver].compact
      # paths = all_paths.compact.join(Gem.path_separator) + Gem.path_separator
      all_paths.map { |p| format_for_platform(p) }
    end

    def self.path_to_chromedriver(platform = nil)
      driver_path = "#{Dir.pwd}/drivers/chromedriver/#{platform || current_chromedriver_platform}"
      make_driver_executable("#{driver_path}/chromedriver")
      driver_path
    end

    def self.current_chromedriver_platform
      case RbConfig::CONFIG['host_os']
      when 'mingw32'
        'win32'
      when 'mac', /darwin/
        'mac32'
      else
        RbConfig::CONFIG['host_cpu'].eql?('x86_64') ? 'linux64' : 'linux32'
      end
    end

    def self.path_to_geckodriver(platform = nil)
      driver_path = "#{Dir.pwd}/drivers/geckodriver/#{platform || current_geckodriver_platform}"
      make_driver_executable("#{driver_path}/geckodriver")
      driver_path
    end

    def self.current_geckodriver_platform
      case RbConfig::CONFIG['host_os']
      when 'mingw32'
        ENV['architecture'].eql?('64') ? 'win64' : 'win32'
      when 'mac', /darwin/
        'macos'
      else
        RbConfig::CONFIG['host_cpu'].eql?('x86_64') ? 'linux64' : 'linux32'
      end
    end

    def self.path_to_iedriver(platform = nil)
      windows? ? "#{Dir.pwd}/drivers/iedriver/#{platform || current_iedriver_platform}" : nil
    end

    def self.current_iedriver_platform
      ENV['architecture'].eql?('64') ? 'x64' : 'Win32'
    end

    def self.path_to_edgedriver(platform = nil)
      return nil unless windows_10?
      p = platform || windows_build_number
      "#{Dir.pwd}/drivers/microsoft_webdriver/#{p}"
    end

    # Takes a file path and puts the appropriate slashes for the operating
    # system.
    def self.format_for_platform(path, platform = nil)
      host_os = platform || RbConfig::CONFIG['host_os']
      if host_os.eql?('mingw32')
        path.tr('/', '\\')
      elsif host_os.include?('linux') || host_os.include?('mac') || host_os.include?('darwin')
        path.tr('\\', '/')
      else
        raise UnknownPlatformError, platform
      end
    end

    def self.windows?
      RbConfig::CONFIG['host_os'].eql?('mingw32')
    end

    def self.windows_10?
      windows? && windows_version.eql?('Version 10')
    end

    def self.windows_version
      `ver`.match(/(Version \d+)/)[1]
    end

    def self.windows_build_number
      `ver`.match(/\.(\d+)\]/)[1]
    end

    def self.make_driver_executable(file_path)
      return if windows?
      File.chmod(0o0777, file_path) if File.exist?(file_path)
    end
  end
end
