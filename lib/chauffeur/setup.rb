class Chauffeur
  # Initializes the folders for storing the drivers and creates config.yml file
  # that stores the versions of installed drivers.
  module Setup
    # Initializes the folders for storing the drivers and creates config.yml file
    # that stores the versions of installed drivers.
    def self.create_folders_and_config
      config_file_path = "#{File.dirname(__FILE__)}/config.yml"
      data = File.open(config_file_path) { |f| YAML.safe_load(f) }
      create_folders(data['folders'])
      create_config(data)
    end

    # Initializes the folders for storing the drivers
    #
    # folders: Array(strings) - each is a folder to be created relative
    #          to the current working directory
    def self.create_folders(folders)
      folders.each do |folder|
        full_path = "#{Dir.pwd}/#{folder}"
        FileUtils.mkdir_p(full_path) unless File.exist?(full_path)
      end
    end

    # Initializes the config.yml file that is used for storing the installed
    # versions of the specific drivers
    #
    # data: hash
    def self.create_config(data)
      return if File.exist?("#{Dir.pwd}/drivers/config.yml")
      File.open("#{Dir.pwd}/drivers/config.yml", 'w') do |file|
        file.puts(data.to_yaml)
      end
    end
  end
end
