# This class gets thrown when the platform is something other than:
#   mingw32
#   mac
#   linux
class UnknownPlatformError < StandardError
  def initialize(platform, msg = nil)
    msg ||= "Unknown OS: #{platform}"
    super(msg)
  end
end
