# This class gets thrown when the platform is something other than valid driver versions.
class UnknownVersionError < StandardError
  def initialize(version, msg = nil)
    msg ||= "Unknown Version: #{version}"
    super(msg)
  end
end
