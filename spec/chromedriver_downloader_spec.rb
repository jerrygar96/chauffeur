require_relative './helpers/spec_helper'

describe 'downloader_for chrome' do
  before(:all) do
    Chauffeur::Setup.create_folders_and_config
  end
  after(:all) do
    FileUtils.rm_r("#{Dir.pwd}/drivers") if File.exist?("#{Dir.pwd}/drivers")
  end

  it 'returns a ChromedriverDownloader when chrome is passed in' do
    downloader = Chauffeur.downloader_for('chrome')
    expect(downloader.is_a?(ChromedriverDownloader)).to be_truthy
  end
end

describe 'driver_download_url mac' do
  before(:all) do
    Chauffeur::Setup.create_folders_and_config
  end
  after(:all) do
    FileUtils.rm_r("#{Dir.pwd}/drivers") if File.exist?("#{Dir.pwd}/drivers")
  end
  it 'returns the url for the mac when mac32 is given' do
    version = '2.11'
    platform = 'mac32'
    downloader = ChromedriverDownloader.new(false)
    url = downloader.driver_download_url(version, platform)
    expect(url).to eq('http://chromedriver.storage.googleapis.com/2.11/chromedriver_mac32.zip')
  end
end

describe 'driver_download_url linux' do
  before(:all) do
    Chauffeur::Setup.create_folders_and_config
    @downloader = ChromedriverDownloader.new(false)
  end
  after(:all) do
    FileUtils.rm_r("#{Dir.pwd}/drivers") if File.exist?("#{Dir.pwd}/drivers")
  end
  it 'returns the url for the linux32 when linux32 is given' do
    version = '2.23'
    platform = 'linux32'
    url = @downloader.driver_download_url(version, platform)
    expect(url).to eq('http://chromedriver.storage.googleapis.com/2.23/chromedriver_linux32.zip')
  end

  it 'returns the url for the linux64 when linux64 is given' do
    version = '2.23'
    platform = 'linux64'
    url = @downloader.driver_download_url(version, platform)
    expect(url).to eq('http://chromedriver.storage.googleapis.com/2.23/chromedriver_linux64.zip')
  end
end

describe 'latest_driver_version' do
  before(:all) do
    Chauffeur::Setup.create_folders_and_config
    @downloader = ChromedriverDownloader.new(false)
  end
  after(:all) do
    FileUtils.rm_r("#{Dir.pwd}/drivers") if File.exist?("#{Dir.pwd}/drivers")
  end
  it 'gives the latest version for current platform when none is given' do
    expected_version = Gem::Version.new('2.37')
    found_version = @downloader.latest_driver_version('win32')
    expect(found_version).to eq(expected_version)
  end

  it 'gives the latest version for windows when win32 is given' do
    platform = 'win32'
    expected_version = Gem::Version.new('2.37')
    found_version = @downloader.latest_driver_version(platform)
    expect(found_version).to eq(expected_version)
  end

  it 'gives the latest version for mac when mac32 is given' do
    expected_version = Gem::Version.new('2.22')
    found_version = @downloader.latest_driver_version('mac32')
    expect(found_version).to eq(expected_version)
  end
end
