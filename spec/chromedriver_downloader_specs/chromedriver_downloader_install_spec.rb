require_relative '../helpers/spec_helper'

describe 'chromedriver downloader install methods' do
  before(:each) do
    Chauffeur::Setup.create_folders_and_config
  end
  after(:each) do
    FileUtils.rm_r("#{Dir.pwd}/drivers") if File.exist?("#{Dir.pwd}/drivers")
  end
  describe 'upgrade_driver' do
    it 'installs a latest driver version for linux32' do
      Chauffeur.upgrade_driver('chrome', 'linux32', false)
      file_exists = File.exist?("#{Chauffeur::PathExpander.path_to_chromedriver('linux32')}/chromedriver")
      version = Chauffeur.driver_versions['chromedriver']['linux32'].to_f > 0
      expect([file_exists, version]).to eq([true, true])
    end
    it 'returns a latest driver version for linux64' do
      Chauffeur.upgrade_driver('chrome', 'linux64', false)
      file_exists = File.exist?("#{Chauffeur::PathExpander.path_to_chromedriver('linux64')}/chromedriver")
      version = Chauffeur.driver_versions['chromedriver']['linux64'].to_f > 0
      expect([file_exists, version]).to eq([true, true])
    end
    it 'returns a latest driver version for mac32' do
      Chauffeur.upgrade_driver('chrome', 'mac32', false)
      file_exists = File.exist?("#{Chauffeur::PathExpander.path_to_chromedriver('mac32')}/chromedriver")
      version = Chauffeur.driver_versions['chromedriver']['mac32'].to_f > 0
      expect([file_exists, version]).to eq([true, true])
    end
    it 'returns a latest driver version for win32' do
      Chauffeur.upgrade_driver('chrome', 'win32', false)
      file_exists = File.exist?("#{Chauffeur::PathExpander.path_to_chromedriver('win32')}/chromedriver.exe")
      version = Chauffeur.driver_versions['chromedriver']['win32'].to_f > 0
      expect([file_exists, version]).to eq([true, true])
    end
    it 'throws an error for unknown platform' do
      e = Object.const_get('UnknownPlatformError')
      expect { Chauffeur.upgrade_driver('chrome', 'linux34', false) }.to raise_error(e)
    end
  end
  describe 'upgrade_driver_all_platforms' do
    it 'installs a latest driver version for linux32' do
      Chauffeur.upgrade_driver_all_platforms('chrome', false)
      file_exists = File.exist?("#{Chauffeur::PathExpander.path_to_chromedriver('linux32')}/chromedriver")
      version = Chauffeur.driver_versions['chromedriver']['linux32'].to_f > 0
      expect([file_exists, version]).to eq([true, true])
    end
    it 'returns a latest driver version for linux64' do
      Chauffeur.upgrade_driver_all_platforms('chrome', false)
      file_exists = File.exist?("#{Chauffeur::PathExpander.path_to_chromedriver('linux64')}/chromedriver")
      version = Chauffeur.driver_versions['chromedriver']['linux64'].to_f > 0
      expect([file_exists, version]).to eq([true, true])
    end
    it 'returns a latest driver version for mac32' do
      Chauffeur.upgrade_driver_all_platforms('chrome', false)
      file_exists = File.exist?("#{Chauffeur::PathExpander.path_to_chromedriver('mac32')}/chromedriver")
      version = Chauffeur.driver_versions['chromedriver']['mac32'].to_f > 0
      expect([file_exists, version]).to eq([true, true])
    end
    it 'returns a latest driver version for win32' do
      Chauffeur.upgrade_driver_all_platforms('chrome', false)
      file_exists = File.exist?("#{Chauffeur::PathExpander.path_to_chromedriver('win32')}/chromedriver.exe")
      version = Chauffeur.driver_versions['chromedriver']['win32'].to_f > 0
      expect([file_exists, version]).to eq([true, true])
    end
  end
  describe 'install_driver' do
    it 'installs a latest driver version for linux32' do
      Chauffeur.install_driver('chrome', 'linux32', '2.2', false)
      file_exists = File.exist?("#{Chauffeur::PathExpander.path_to_chromedriver('linux32')}/chromedriver")
      version = Chauffeur.driver_versions['chromedriver']['linux32'].to_f > 0
      expect([file_exists, version]).to eq([true, true])
    end
    it 'returns a latest driver version for linux64' do
      Chauffeur.install_driver('chrome', 'linux64', '2.2', false)
      file_exists = File.exist?("#{Chauffeur::PathExpander.path_to_chromedriver('linux64')}/chromedriver")
      version = Chauffeur.driver_versions['chromedriver']['linux64'].to_f > 0
      expect([file_exists, version]).to eq([true, true])
    end
    it 'returns a latest driver version for mac32' do
      Chauffeur.install_driver('chrome','mac32', '2.2', false)
      file_exists = File.exist?("#{Chauffeur::PathExpander.path_to_chromedriver('mac32')}/chromedriver")
      version = Chauffeur.driver_versions['chromedriver']['mac32'].to_f > 0
      expect([file_exists, version]).to eq([true, true])
    end
    it 'returns a latest driver version for win32' do
      Chauffeur.install_driver('chrome', 'win32', '2.2', false)
      file_exists = File.exist?("#{Chauffeur::PathExpander.path_to_chromedriver('win32')}/chromedriver.exe")
      version = Chauffeur.driver_versions['chromedriver']['win32'].to_f > 0
      expect([file_exists, version]).to eq([true, true])
    end
    it 'does not install driver if it is already the currently installed driver' do
      Chauffeur.install_driver('chrome', 'linux32', '2.2', false)
      installed = Chauffeur.install_driver('chrome', 'linux32', '2.2')
      expect(installed).to be(false)
    end
    it 'throws an error for unknown platform' do
      e = Object.const_get('UnknownPlatformError')
      expect { Chauffeur.install_driver('chrome', '2.2', 'linux34') }.to raise_error(e)
    end
  end
  describe 'install_driver_all_platforms' do
    it 'installs a latest driver version for linux32' do
      Chauffeur.install_driver_all_platforms('chrome', '2.2', false)
      file_exists = File.exist?("#{Chauffeur::PathExpander.path_to_chromedriver('linux32')}/chromedriver")
      version = Chauffeur.driver_versions['chromedriver']['linux32'].to_f > 0
      expect([file_exists, version]).to eq([true, true])
    end
    it 'returns a latest driver version for linux64' do
      Chauffeur.install_driver_all_platforms('chrome', '2.2', false)
      file_exists = File.exist?("#{Chauffeur::PathExpander.path_to_chromedriver('linux64')}/chromedriver")
      version = Chauffeur.driver_versions['chromedriver']['linux64'].to_f > 0
      expect([file_exists, version]).to eq([true, true])
    end
    it 'returns a latest driver version for mac32' do
      Chauffeur.install_driver_all_platforms('chrome', '2.2', false)
      file_exists = File.exist?("#{Chauffeur::PathExpander.path_to_chromedriver('mac32')}/chromedriver")
      version = Chauffeur.driver_versions['chromedriver']['mac32'].to_f > 0
      expect([file_exists, version]).to eq([true, true])
    end
    it 'returns a latest driver version for win32' do
      Chauffeur.install_driver_all_platforms('chrome', '2.2', false)
      file_exists = File.exist?("#{Chauffeur::PathExpander.path_to_chromedriver('win32')}/chromedriver.exe")
      version = Chauffeur.driver_versions['chromedriver']['win32'].to_f > 0
      expect([file_exists, version]).to eq([true, true])
    end
  end
end