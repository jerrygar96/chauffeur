require_relative '../helpers/spec_helper'

describe 'chromedriver downloader single line methods' do
  before(:all) do
    Chauffeur::Setup.create_folders_and_config
    @downloader = ChromedriverDownloader.new(false)
  end
  after(:all) do
    FileUtils.rm_r("#{Dir.pwd}/drivers") if File.exist?("#{Dir.pwd}/drivers")
  end
  it 'has a browser name' do
    expect(@downloader.browser_name).to eq('chromedriver')
  end
  it 'has a browser url' do
    expected = 'http://chromedriver.storage.googleapis.com/'
    expect(@downloader.driver_url).to eq(expected)
  end
  it 'has a browser url' do
    expected = %w[linux32 linux64 mac32 win32]
    expect(@downloader.all_platforms).to eq(expected)
  end
end
