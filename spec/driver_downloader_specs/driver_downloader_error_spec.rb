require_relative '../helpers/spec_helper'

describe 'driver downloader error throwing methods' do
  before(:all) do
    Chauffeur::Setup.create_folders_and_config
    @downloader = DriverDownloader.new
  end
  after(:all) do
    FileUtils.rm_r("#{Dir.pwd}/drivers") if File.exist?("#{Dir.pwd}/drivers")
  end
  it 'throws an error for browser_name' do
    expect { @downloader.browser_name }.to raise_error(RuntimeError)
  end
  it 'throws an error for driver_url' do
    expect { @downloader.driver_url }.to raise_error(RuntimeError)
  end
  it 'throws an error for all_platforms' do
    expect { @downloader.all_platforms }.to raise_error(RuntimeError)
  end
  it 'throws an error for latest_driver_version' do
    expect { @downloader.latest_driver_version('something') }.to raise_error(RuntimeError)
  end
  it 'throws an error for all_driver_versions' do
    expect { @downloader.all_driver_versions }.to raise_error(RuntimeError)
  end
  it 'throws an error for driver_download_url' do
    expect { @downloader.driver_download_url('something', 'test') }.to raise_error(RuntimeError)
  end
end
