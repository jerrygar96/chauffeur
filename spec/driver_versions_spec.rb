require_relative './helpers/spec_helper'

describe 'driver versions' do
  before(:all) do
    Chauffeur::Setup.create_folders_and_config
  end
  after(:all) do
    FileUtils.rm_r("#{Dir.pwd}/drivers")
  end
  it 'returns a hash of all installed versions' do
    expected = { 'chromedriver' =>
                    { 'linux32' => 'nil',
                      'linux64' => 'nil',
                      'mac32' => 'nil',
                      'win32' => 'nil' },
                 'geckodriver' =>
                    { 'linux32' => 'nil',
                      'linux64' => 'nil',
                      'macos' => 'nil',
                      'win32' => 'nil',
                      'win64' => 'nil' },
                 'iedriver' =>
                    { 'Win32' => 'nil',
                      'x64' => 'nil' },
                 'microsoft_webdriver' => {} }
    found = Chauffeur.driver_versions
    expect(found).to eq(expected)
  end
end
