require_relative '../helpers/spec_helper'

describe 'edgedriver downloader install methods' do
  before(:each) do
    Chauffeur::Setup.create_folders_and_config
  end
  after(:each) do
    FileUtils.rm_r("#{Dir.pwd}/drivers") if File.exist?("#{Dir.pwd}/drivers")
  end
  describe 'upgrade_driver' do
    it 'installs a latest driver version for edge' do
      Chauffeur.upgrade_driver('edge', 'win', false)
      latest = Chauffeur.downloader_for('edge').latest_driver_version('win')
      file_exists = File.exist?("#{Chauffeur::PathExpander.path_to_edgedriver(latest)}/MicrosoftWebDriver.exe")
      version = Chauffeur.driver_versions['microsoft_webdriver']['win'].include?(latest)
      expect([file_exists, version]).to eq([true, true])
    end
  end
  describe 'upgrade_driver_all_platforms' do
    it 'installs a latest driver version for edge' do
      Chauffeur.upgrade_driver_all_platforms('edge', false)
      latest = Chauffeur.downloader_for('edge').latest_driver_version('win')
      file_exists = File.exist?("#{Chauffeur::PathExpander.path_to_edgedriver(latest)}/MicrosoftWebDriver.exe")
      version = Chauffeur.driver_versions['microsoft_webdriver']['win'].include?(latest)
      expect([file_exists, version]).to eq([true, true])
    end
  end
  describe 'install_driver' do
    it 'installs a driver version for edge' do
      version = '15063'
      Chauffeur.install_driver('edge', 'win', version,false)
      file_exists = File.exist?("#{Chauffeur::PathExpander.path_to_edgedriver(version)}/MicrosoftWebDriver.exe")
      version = Chauffeur.driver_versions['microsoft_webdriver']['win'].include?(version)
      expect([file_exists, version]).to eq([true, true])
    end
    it 'does not install driver if it is already the currently installed driver' do
      Chauffeur.install_driver('edge', 'win', '10586', false)
      installed = Chauffeur.install_driver('edge', 'win', '10586', false)
      expect(installed).to be(false)
    end
    it 'throws an error for unknown platform' do
      e = Object.const_get('UnknownPlatformError')
      expect { Chauffeur.install_driver('edge', '2.2', 'win') }.to raise_error(e)
    end
  end
  describe 'install_driver_all_platforms' do
    it 'installs a driver version for edge' do
      version = '15063'
      Chauffeur.install_driver_all_platforms('edge', version,false)
      file_exists = File.exist?("#{Chauffeur::PathExpander.path_to_edgedriver(version)}/MicrosoftWebDriver.exe")
      version = Chauffeur.driver_versions['microsoft_webdriver']['win'].include?(version)
      expect([file_exists, version]).to eq([true, true])
    end
  end
end