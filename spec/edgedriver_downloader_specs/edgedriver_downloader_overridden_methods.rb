require_relative '../helpers/spec_helper'

describe 'chromedriver downloader overridden methods' do
  before(:all) do
    Chauffeur::Setup.create_folders_and_config
    @downloader = EdgedriverDownloader.new
  end
  after(:all) do
    FileUtils.rm_r("#{Dir.pwd}/drivers") if File.exist?("#{Dir.pwd}/drivers")
  end
  describe 'latest_driver_version' do
    it 'returns a latest driver version for win' do
      expect(@downloader.latest_driver_version('win')).to be_a(String)
    end
    it 'throws an error for unknown platform' do
      e = Object.const_get('UnknownPlatformError')
      expect { @downloader.latest_driver_version('linux33') }.to raise_error(e)
    end
  end
end
describe 'all versions and errors for driver_download_url' do
  before(:all) do
    Chauffeur::Setup.create_folders_and_config
    @downloader = EdgedriverDownloader.new
  end
  after(:all) do
    FileUtils.rm_r("#{Dir.pwd}/drivers") if File.exist?("#{Dir.pwd}/drivers")
  end
  describe 'all_driver_versions' do
    it 'returns an array for all_driver_versions' do
      found = @downloader.all_driver_versions
      expect(found).to be_a(Hash)
    end
  end
  describe 'driver_download_url' do
    it 'raises an Unknown platform error for unknown platform' do
      e = Object.const_get('UnknownPlatformError')
      expect { @downloader.driver_download_url('2.3', 'linux33') }.to raise_error(e)
    end
    it 'raises an Unknown Version Error When version is invalid' do
      e = Object.const_get('UnknownVersionError')
      expect { @downloader.driver_download_url('1.132', 'win') }.to raise_error(e)
    end
  end
end
describe 'driver_download_url' do
  before(:all) do
    Chauffeur::Setup.create_folders_and_config
    @downloader = EdgedriverDownloader.new
  end
  after(:all) do
    FileUtils.rm_r("#{Dir.pwd}/drivers") if File.exist?("#{Dir.pwd}/drivers")
  end
  it 'returns a string for valid version and win' do
    found = @downloader.driver_download_url('15063', 'win')
    expect(found).to be_a(String)
  end
end
