require_relative '../helpers/spec_helper'

describe 'chromedriver downloader single line methods' do
  before(:all) do
    Chauffeur::Setup.create_folders_and_config
    @downloader = EdgedriverDownloader.new
  end
  after(:all) do
    FileUtils.rm_r("#{Dir.pwd}/drivers") if File.exist?("#{Dir.pwd}/drivers")
  end
  it 'has a browser name' do
    expect(@downloader.browser_name).to eq('microsoft_webdriver')
  end
  it 'has a browser url' do
    expected = 'https://developer.microsoft.com/en-us/microsoft-edge/tools/webdriver/'
    expect(@downloader.driver_url).to eq(expected)
  end
  it 'has a browser url' do
    expected = %w[win]
    expect(@downloader.all_platforms).to eq(expected)
  end
end
