require_relative '../helpers/spec_helper'

describe 'firefox downloader install methods' do
  before(:each) do
    Chauffeur::Setup.create_folders_and_config
  end
  after(:each) do
    FileUtils.rm_r("#{Dir.pwd}/drivers") if File.exist?("#{Dir.pwd}/drivers")
  end
  describe 'upgrade_driver' do
    it 'installs a latest driver version for linux32' do
      Chauffeur.upgrade_driver('firefox', 'linux32', false)
      file_exists = File.exist?("#{Chauffeur::PathExpander.path_to_geckodriver('linux32')}/geckodriver")
      version = Chauffeur.driver_versions['geckodriver']['linux32'].to_f > 0
      expect([file_exists, version]).to eq([true, true])
    end
    it 'returns a latest driver version for linux64' do
      Chauffeur.upgrade_driver('firefox', 'linux64', false)
      file_exists = File.exist?("#{Chauffeur::PathExpander.path_to_geckodriver('linux64')}/geckodriver")
      version = Chauffeur.driver_versions['geckodriver']['linux64'].to_f > 0
      expect([file_exists, version]).to eq([true, true])
    end
    it 'returns a latest driver version for macos' do
      Chauffeur.upgrade_driver('firefox', 'macos', false)
      file_exists = File.exist?("#{Chauffeur::PathExpander.path_to_geckodriver('macos')}/geckodriver")
      version = Chauffeur.driver_versions['geckodriver']['macos'].to_f > 0
      expect([file_exists, version]).to eq([true, true])
    end
    it 'returns a latest driver version for win32' do
      Chauffeur.upgrade_driver('firefox', 'win32', false)
      file_exists = File.exist?("#{Chauffeur::PathExpander.path_to_geckodriver('win32')}/geckodriver.exe")
      version = Chauffeur.driver_versions['geckodriver']['win32'].to_f > 0
      expect([file_exists, version]).to eq([true, true])
    end
    it 'returns a latest driver version for win64' do
      Chauffeur.upgrade_driver('firefox', 'win64', false)
      file_exists = File.exist?("#{Chauffeur::PathExpander.path_to_geckodriver('win64')}/geckodriver.exe")
      version = Chauffeur.driver_versions['geckodriver']['win64'].to_f > 0
      expect([file_exists, version]).to eq([true, true])
    end
    it 'throws an error for unknown platform' do
      e = Object.const_get('UnknownPlatformError')
      expect { Chauffeur.upgrade_driver('firefox', 'linux34') }.to raise_error(e)
    end
  end
  describe 'upgrade_driver_all_platforms' do
    it 'installs a latest driver version for linux32' do
      Chauffeur.upgrade_driver_all_platforms('firefox', false)
      file_exists = File.exist?("#{Chauffeur::PathExpander.path_to_geckodriver('linux32')}/geckodriver")
      version = Chauffeur.driver_versions['geckodriver']['linux32'].to_f > 0
      expect([file_exists, version]).to eq([true, true])
    end
    it 'returns a latest driver version for linux64' do
      Chauffeur.upgrade_driver_all_platforms('firefox', false)
      file_exists = File.exist?("#{Chauffeur::PathExpander.path_to_geckodriver('linux64')}/geckodriver")
      version = Chauffeur.driver_versions['geckodriver']['linux64'].to_f > 0
      expect([file_exists, version]).to eq([true, true])
    end
    it 'returns a latest driver version for macos' do
      Chauffeur.upgrade_driver_all_platforms('firefox', false)
      file_exists = File.exist?("#{Chauffeur::PathExpander.path_to_geckodriver('macos')}/geckodriver")
      version = Chauffeur.driver_versions['geckodriver']['macos'].to_f > 0
      expect([file_exists, version]).to eq([true, true])
    end
    it 'returns a latest driver version for win32' do
      Chauffeur.upgrade_driver_all_platforms('firefox', false)
      file_exists = File.exist?("#{Chauffeur::PathExpander.path_to_geckodriver('win32')}/geckodriver.exe")
      version = Chauffeur.driver_versions['geckodriver']['win32'].to_f > 0
      expect([file_exists, version]).to eq([true, true])
    end
    it 'returns a latest driver version for win64' do
      Chauffeur.upgrade_driver_all_platforms('firefox', false)
      file_exists = File.exist?("#{Chauffeur::PathExpander.path_to_geckodriver('win64')}/geckodriver.exe")
      version = Chauffeur.driver_versions['geckodriver']['win64'].to_f > 0
      expect([file_exists, version]).to eq([true, true])
    end
  end
  describe 'install_driver' do
    it 'installs a latest driver version for linux32' do
      Chauffeur.install_driver('firefox', 'linux32', '0.23.0', false)
      file_exists = File.exist?("#{Chauffeur::PathExpander.path_to_geckodriver('linux32')}/geckodriver")
      version = Chauffeur.driver_versions['geckodriver']['linux32'].to_f > 0
      expect([file_exists, version]).to eq([true, true])
    end
    it 'returns a latest driver version for linux64' do
      Chauffeur.install_driver('firefox', 'linux64', '0.23.0', false)
      file_exists = File.exist?("#{Chauffeur::PathExpander.path_to_geckodriver('linux64')}/geckodriver")
      version = Chauffeur.driver_versions['geckodriver']['linux64'].to_f > 0
      expect([file_exists, version]).to eq([true, true])
    end
    it 'returns a latest driver version for macos' do
      Chauffeur.install_driver('firefox','macos', '0.23.0', false)
      file_exists = File.exist?("#{Chauffeur::PathExpander.path_to_geckodriver('macos')}/geckodriver")
      version = Chauffeur.driver_versions['geckodriver']['macos'].to_f > 0
      expect([file_exists, version]).to eq([true, true])
    end
    it 'returns a latest driver version for win32' do
      Chauffeur.install_driver('firefox', 'win32', '0.23.0', false)
      file_exists = File.exist?("#{Chauffeur::PathExpander.path_to_geckodriver('win32')}/geckodriver.exe")
      version = Chauffeur.driver_versions['geckodriver']['win32'].to_f > 0
      expect([file_exists, version]).to eq([true, true])
    end
    it 'returns a latest driver version for win64' do
      Chauffeur.install_driver('firefox', 'win64', '0.23.0', false)
      file_exists = File.exist?("#{Chauffeur::PathExpander.path_to_geckodriver('win64')}/geckodriver.exe")
      version = Chauffeur.driver_versions['geckodriver']['win64'].to_f > 0
      expect([file_exists, version]).to eq([true, true])
    end
    it 'does not install driver if it is already the currently installed driver' do
      Chauffeur.install_driver('firefox', 'linux32', '0.18.0', false)
      installed = Chauffeur.install_driver('firefox', 'linux32', '0.18.0')
      expect(installed).to be(false)
    end
    it 'throws an error for unknown platform' do
      e = Object.const_get('UnknownPlatformError')
      expect { Chauffeur.install_driver('firefox', '0.18.0', 'linux34') }.to raise_error(e)
    end
  end
  describe 'install_driver_all_platforms' do
    it 'installs a latest driver version for linux32' do
      Chauffeur.install_driver_all_platforms('firefox', '0.18.0', false)
      file_exists = File.exist?("#{Chauffeur::PathExpander.path_to_geckodriver('linux32')}/geckodriver")
      version = Chauffeur.driver_versions['geckodriver']['linux32'].to_f > 0
      expect([file_exists, version]).to eq([true, true])
    end
    it 'returns a latest driver version for linux64' do
      Chauffeur.install_driver_all_platforms('firefox', '0.18.0', false)
      file_exists = File.exist?("#{Chauffeur::PathExpander.path_to_geckodriver('linux64')}/geckodriver")
      version = Chauffeur.driver_versions['geckodriver']['linux64'].to_f > 0
      expect([file_exists, version]).to eq([true, true])
    end
    it 'returns a latest driver version for macos' do
      Chauffeur.install_driver_all_platforms('firefox', '0.18.0', false)
      file_exists = File.exist?("#{Chauffeur::PathExpander.path_to_geckodriver('macos')}/geckodriver")
      version = Chauffeur.driver_versions['geckodriver']['macos'].to_f > 0
      expect([file_exists, version]).to eq([true, true])
    end
    it 'returns a latest driver version for win32' do
      Chauffeur.install_driver_all_platforms('firefox', '0.18.0', false)
      file_exists = File.exist?("#{Chauffeur::PathExpander.path_to_geckodriver('win32')}/geckodriver.exe")
      version = Chauffeur.driver_versions['geckodriver']['win32'].to_f > 0
      expect([file_exists, version]).to eq([true, true])
    end
    it 'returns a latest driver version for win64' do
      Chauffeur.install_driver_all_platforms('firefox', '0.18.0', false)
      file_exists = File.exist?("#{Chauffeur::PathExpander.path_to_geckodriver('win64')}/geckodriver.exe")
      version = Chauffeur.driver_versions['geckodriver']['win64'].to_f > 0
      expect([file_exists, version]).to eq([true, true])
    end
  end
end