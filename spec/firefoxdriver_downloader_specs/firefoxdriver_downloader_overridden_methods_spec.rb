require_relative '../helpers/spec_helper'

describe 'firefoxdriver downloader overridden methods' do
  before(:all) do
    Chauffeur::Setup.create_folders_and_config
    @downloader = FirefoxdriverDownloader.new
  end
  after(:all) do
    FileUtils.rm_r("#{Dir.pwd}/drivers") if File.exist?("#{Dir.pwd}/drivers")
  end
  describe 'latest_driver_version' do
    it 'returns a latest driver version for linux32' do
      expect(@downloader.latest_driver_version('linux32')).to be_a(Gem::Version)
    end
    it 'returns a latest driver version for linux64' do
      expect(@downloader.latest_driver_version('linux64')).to be_a(Gem::Version)
    end
    it 'returns a latest driver version for macos' do
      expect(@downloader.latest_driver_version('macos')).to be_a(Gem::Version)
    end
    it 'returns a latest driver version for win32' do
      expect(@downloader.latest_driver_version('win32')).to be_a(Gem::Version)
    end
    it 'returns a latest driver version for win64' do
      expect(@downloader.latest_driver_version('win64')).to be_a(Gem::Version)
    end
    it 'throws an error for unknown platform' do
      e = Object.const_get('UnknownPlatformError')
      expect { @downloader.latest_driver_version('linux33') }.to raise_error(e)
    end
  end
end
describe 'all versions and errors for driver_download_url' do
  before(:all) do
    Chauffeur::Setup.create_folders_and_config
    @downloader = FirefoxdriverDownloader.new
  end
  after(:all) do
    FileUtils.rm_r("#{Dir.pwd}/drivers") if File.exist?("#{Dir.pwd}/drivers")
  end
  describe 'all_driver_versions' do
    it 'returns an array for all_driver_versions' do
      found = @downloader.all_driver_versions
      expect(found).to be_a(Array)
    end
  end
  describe 'driver_download_url' do
    it 'raises an Unknown platform error for unknown platform' do
      e = Object.const_get('UnknownPlatformError')
      expect { @downloader.driver_download_url('0.18.0', 'linux33') }.to raise_error(e)
    end
    it 'raises an Unknown Version Error When version is invalid' do
      e = Object.const_get('UnknownVersionError')
      expect { @downloader.driver_download_url('1.132', 'linux32') }.to raise_error(e)
    end
  end
end
describe 'driver_download_url' do
  before(:all) do
    Chauffeur::Setup.create_folders_and_config
    @downloader = FirefoxdriverDownloader.new
  end
  after(:all) do
    FileUtils.rm_r("#{Dir.pwd}/drivers") if File.exist?("#{Dir.pwd}/drivers")
  end
  it 'returns a string for valid version and linux32' do
    found = @downloader.driver_download_url('0.18.0', 'linux32')
    expect(found).to be_a(String)
  end
  it 'returns a string for valid version and linux64' do
    found = @downloader.driver_download_url('0.18.0', 'linux64')
    expect(found).to be_a(String)
  end
  it 'returns a string for valid version and macos' do
    found = @downloader.driver_download_url('0.18.0', 'macos')
    expect(found).to be_a(String)
  end
  it 'returns a string for valid version and win64' do
    found = @downloader.driver_download_url('0.18.0', 'win64')
    expect(found).to be_a(String)
  end
  it 'returns a string for valid version and win32' do
    found = @downloader.driver_download_url('0.18.0', 'win32')
    expect(found).to be_a(String)
  end
end
