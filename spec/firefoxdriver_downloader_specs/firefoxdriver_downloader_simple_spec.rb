require_relative '../helpers/spec_helper'

describe 'firefoxdriver downloader single line methods' do
  before(:all) do
    Chauffeur::Setup.create_folders_and_config
    @downloader = FirefoxdriverDownloader.new
  end
  after(:all) do
    FileUtils.rm_r("#{Dir.pwd}/drivers") if File.exist?("#{Dir.pwd}/drivers")
  end
  it 'has a browser name' do
    expect(@downloader.browser_name).to eq('geckodriver')
  end
  it 'has a browser url' do
    expected = 'https://github.com/mozilla/geckodriver/releases/'
    expect(@downloader.driver_url).to eq(expected)
  end
  it 'has a browser url' do
    expected = %w[linux32 linux64 macos win32 win64]
    expect(@downloader.all_platforms).to eq(expected)
  end
end
