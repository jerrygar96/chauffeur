require_relative './helpers/spec_helper'

describe 'formatting paths correctly windows' do
  it 'keeps back slashes on windows' do
    test_path = 'C:\\some\\fake\\path'
    formatted_path = Chauffeur::PathExpander.format_for_platform(test_path, 'mingw32')
    expect(formatted_path).to eq(test_path)
  end

  it 'trades forward slashes for back slashes on windows' do
    test_path = 'C:/some/fake/path'
    expected_path = 'C:\\some\\fake\\path'
    formatted_path = Chauffeur::PathExpander.format_for_platform(test_path, 'mingw32')
    expect(formatted_path).to eq(expected_path)
  end
end

describe 'formatting paths correctly linux' do
  it 'keeps forward slashes on linux' do
    test_path = '/some/fake/path'
    formatted_path = Chauffeur::PathExpander.format_for_platform(test_path, 'linux')
    expect(formatted_path).to eq(test_path)
  end

  it 'trades back slashes for forward slashes on linux' do
    test_path = '\\some\\fake\\path'
    expected_path = '/some/fake/path'
    formatted_path = Chauffeur::PathExpander.format_for_platform(test_path, 'linux')
    expect(formatted_path).to eq(expected_path)
  end
end

describe 'formatting paths correctly mac' do
  it 'keeps forward slashes on mac' do
    test_path = '/some/fake/path'
    formatted_path = Chauffeur::PathExpander.format_for_platform(test_path, 'mac')
    expect(formatted_path).to eq(test_path)
  end

  it 'trades back slashes for forward slashes on mac' do
    test_path = '\\some\\fake\\path'
    expected_path = '/some/fake/path'
    formatted_path = Chauffeur::PathExpander.format_for_platform(test_path, 'mac')
    expect(formatted_path).to eq(expected_path)
  end
end

describe 'handling other (unknown) platforms' do
  it 'throws an error on empty platform' do
    test_path = '/some/fake/path'
    e = Object.const_get('UnknownPlatformError')
    expect { Chauffeur::PathExpander.format_for_platform(test_path, '') }.to raise_error(e)
  end
  it 'throws an error on unknown platform' do
    test_path = '/some/fake/path'
    e = Object.const_get('UnknownPlatformError')
    expect { Chauffeur::PathExpander.format_for_platform(test_path, 'mint') }.to raise_error(e)
  end
end
