require_relative './helpers/spec_helper'

describe 'iedriver downloader' do
  before(:all) do
    Chauffeur::Setup.create_folders_and_config
  end
  after(:all) do
    FileUtils.rm_r("#{Dir.pwd}/drivers") if File.exist?("#{Dir.pwd}/drivers")
  end

  describe 'downloader_for ie' do
    it 'returns a IedriverDownloader when ie is passed in' do
      downloader = Chauffeur.downloader_for('ie')
      expect(downloader).to be_a(IedriverDownloader)
    end
  end
end
