require_relative '../helpers/spec_helper'

describe 'iedriver downloader single line methods' do
  before(:all) do
    Chauffeur::Setup.create_folders_and_config
    @downloader = IedriverDownloader.new
  end
  after(:all) do
    FileUtils.rm_r("#{Dir.pwd}/drivers") if File.exist?("#{Dir.pwd}/drivers")
  end
  it 'has a browser name' do
    expect(@downloader.browser_name).to eq('iedriver')
  end
  it 'has a browser url' do
    expected = 'http://selenium-release.storage.googleapis.com/'
    expect(@downloader.driver_url).to eq(expected)
  end
  it 'has a browser url' do
    expected = %w[Win32 x64]
    expect(@downloader.all_platforms).to eq(expected)
  end
end
