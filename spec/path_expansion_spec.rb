require_relative './helpers/spec_helper'

describe 'path expansion' do
  it 'adds relative location to path when no arguments are given' do
    platform = Chauffeur::PathExpander.current_chromedriver_platform
    expansion = "#{Dir.pwd}/drivers/chromedriver/#{platform}#{Gem.path_separator}"
    Chauffeur.add_drivers_to_path
    expect(ENV['PATH']).to include(Chauffeur::PathExpander.format_for_platform(expansion))
  end
end

describe 'path creation' do
  it 'returns the correct path to chromedriver' do
    platform = Chauffeur::PathExpander.current_chromedriver_platform
    expected = "#{Dir.pwd}/drivers/chromedriver/#{platform}"
    found = Chauffeur::PathExpander.path_to_chromedriver
    expect(found).to eq(expected)
  end

  it 'returns the correct path to geckodriver' do
    platform = Chauffeur::PathExpander.current_geckodriver_platform
    expected = "#{Dir.pwd}/drivers/geckodriver/#{platform}"
    found = Chauffeur::PathExpander.path_to_geckodriver
    expect(found).to eq(expected)
  end

  it 'returns the correct path to iedriver' do
    if Chauffeur::PathExpander.windows?
      platform = Chauffeur::PathExpander.current_iedriver_platform
      expected = "#{Dir.pwd}/drivers/iedriver/#{platform}"
    end
    found = Chauffeur::PathExpander.path_to_iedriver
    expect(found).to eq(expected)
  end

  it 'returns the correct path to edgedriver' do
    expected = if Chauffeur::PathExpander.windows_10?
                 ver = Chauffeur::PathExpander.windows_build_number
                 "#{Dir.pwd}/drivers/microsoft_webdriver/#{ver}"
               end
    expect(Chauffeur::PathExpander.path_to_edgedriver).to eq(expected)
  end
end
