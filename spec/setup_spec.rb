require_relative './helpers/spec_helper'

CONFIG_DATA = File.open('./lib/chauffeur/config.yml') { |f| YAML.safe_load(f) }
DRIVERS_DIR = "#{Dir.pwd}/drivers".freeze

describe 'folder creation' do
  before(:each) do
    FileUtils.rm_r(DRIVERS_DIR) if File.exist?(DRIVERS_DIR)
  end
  after(:each) do
    FileUtils.rm_r(DRIVERS_DIR) if File.exist?(DRIVERS_DIR)
  end

  it 'creates folders if they do not exist' do
    folder = 'drivers/test_folder'
    Chauffeur::Setup.create_folders([folder])
    expect(File.exist?(folder)).to be_truthy
  end
end

describe 'config file creation' do
  before(:each) do
    FileUtils.rm_r(DRIVERS_DIR) if File.exist?(DRIVERS_DIR)
  end
  after(:each) do
    FileUtils.rm_r(DRIVERS_DIR) if File.exist?(DRIVERS_DIR)
  end
  it 'does not create file when it already exists' do
    FileUtils.mkdir(DRIVERS_DIR)
    expected_data = { 'new_stuff' => 'new_stuff stored' }
    Chauffeur::Setup.create_config(expected_data)
    Chauffeur::Setup.create_config(CONFIG_DATA)
    config_file_path = './drivers/config.yml'
    found_data = File.open(config_file_path) { |f| YAML.safe_load(f) }
    expect(found_data['new_stuff']).to eq('new_stuff stored')
  end

  it 'creates config.yml file in ./drivers when it does not exist' do
    FileUtils.mkdir(DRIVERS_DIR)
    Chauffeur::Setup.create_config(CONFIG_DATA)
    expect(File.exist?("#{DRIVERS_DIR}/config.yml")).to be_truthy
  end
end

describe 'chauffeur initialize_chauffeur' do
  before(:all) do
    FileUtils.rm_r(DRIVERS_DIR) if File.exist?(DRIVERS_DIR)
    Chauffeur.initialize_chauffeur
  end
  after(:all) do
    FileUtils.rm_r(DRIVERS_DIR) if File.exist?(DRIVERS_DIR)
  end

  it 'creates folders if they do not exist' do
    expect(File.exist?(DRIVERS_DIR)).to be_truthy
  end

  it 'creates config.yml file in ./drivers when it does not exist' do
    expect(File.exist?("#{DRIVERS_DIR}/config.yml")).to be_truthy
  end
end
